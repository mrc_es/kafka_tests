
## Setup

```bash
$ docker-compose up -d
```

And wait few minutes before enter with:

```bash
$ docker-compose exec docker-compose ksqldb-cli ksql http://ksqldb-server:8088
$ docker exec -it broker bash
```

## Creating a test stream and a partition

```sql
SET 'auto.offset.reset'='earliest';
SET 'ksql.streams.cache.max.bytes.buffering'='100000000';

CREATE STREAM st_test(
   field1 BIGINT KEY,
   field2 BIGINT,
   field3 VARCHAR)
WITH (KAFKA_TOPIC='test',
      PARTITIONS=2, 
      REPLICAS=1,
      VALUE_FORMAT='DELIMITED');

select * from st_test emit changes;

```

## Usefull aliases

Some usefull aliases are in [this file](kafka_data/aliases.bash).

**NOTE**: change the broker name as you want. In my case, I'll change to "broker1" and "broker2", respectively inside each broker.

Then, just enter into the broker container:

```bash
$ docker exec -it broker1 bash

# Now, we're inside the broker container
# Let's load our aliases
[appuser@broker ~]$ . /mnt/kafka_data/aliases.bash

# Now we can type commands easily
[appuser@broker ~]$ printf "Hola mundo1\nHola mundo2\n" | kproducer test10
[appuser@broker ~]$ kconsumer test10
Hola mundo1
Hola mundo2
[appuser@broker ~]$ kcount-records test10
2 records in "test10" topic
[appuser@broker ~]$ kdelete-topic test10
[appuser@broker ~]$ kcount-records test10
Topic test10 does not exist
0 records in "test10" topic
```

## Example

<img src="result1.PNG" style="zoom:50%;" />