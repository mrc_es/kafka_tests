
#Run with . <path to this file>
#or source <path to this file>

declare -a broker_list=(
   broker2:29092  #  Place one or multiple brokers
                 #+ e.g. broker2:9092
)
export broker_list_comma_separated="$(IFS=, ; echo "${broker_list[*]}")"

alias kcreate-topic="kafka-topics --bootstrap-server $broker_list_comma_separated --create --topic"
alias kdelete-topic="kafka-topics --bootstrap-server $broker_list_comma_separated --delete --topic"
alias kdescribe-topic="kafka-topics --bootstrap-server $broker_list_comma_separated --describe --topic"
alias klist-topics="kafka-topics --bootstrap-server $broker_list_comma_separated --list"

alias kconsumer="kafka-console-consumer --bootstrap-server $broker_list_comma_separated --from-beginning --topic"
alias kproducer="kafka-console-producer --bootstrap-server $broker_list_comma_separated --topic"

alias zoocommand="zookeeper-shell zookeeper:2181"

kcount-records() {

  declare topic="$1"

  declare records="$(kafka-run-class kafka.tools.GetOffsetShell \
                    --broker-list "$broker_list_comma_separated" \
                    --topic "$topic" \
                    --time -1 \
                    --offsets 1 \
                    | awk -F ":" '{sum+=$3} END{print sum}')"
  records="${records:=0}"

  printf '%s records in \"%s\" topic\n' "$records" "$topic"

}
